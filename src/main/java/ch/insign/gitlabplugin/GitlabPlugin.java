package ch.insign.gitlabplugin;

import ch.insign.gitlabplugin.models.GitlabSettings;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.webfragment.contextproviders.AbstractJiraContextProvider;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.gitlab.api.GitlabAPI;
import org.gitlab.api.models.GitlabBranch;
import org.gitlab.api.models.GitlabProject;
import org.gitlab.api.models.GitlabSession;

import javax.inject.Inject;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.*;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;

public class GitlabPlugin extends AbstractJiraContextProvider
{
    private String url = "";
    private String login = "";
    private String password = "";

    @ComponentImport
    private final ActiveObjects ao;

    @Inject
    public GitlabPlugin(ActiveObjects ao)
    {
        this.ao = checkNotNull(ao);
    }

    @Override
    public Map<String, Object> getContextMap(ApplicationUser user, JiraHelper jiraHelper) {

        GitlabSettings[] gitlabSettings = ao.find(GitlabSettings.class);

        if(gitlabSettings.length > 0){
            url = gitlabSettings[0].getUrl();
            login = gitlabSettings[0].getUsername();
            password = gitlabSettings[0].getPassword();
        }

        Map<String, Object> contextMap = new HashMap<>();

        Issue currentIssue = (Issue) jiraHelper.getContextParams().get("issue");
        String issuekey = currentIssue.getKey();

        try {
            GitlabSession session = GitlabAPI.connect(url, login, password);

            GitlabAPI api = GitlabAPI.connect(gitlabSettings[0].getUrl(), session.getPrivateToken());

            GitlabProject project = getProject(api, currentIssue);

            List<GitlabBranch> branches = getListBranches(api, project, currentIssue);

            contextMap.put("projectkey", currentIssue.getProjectObject().getKey());
            contextMap.put("issuekey", issuekey);
            contextMap.put("branches", branches.size());
            contextMap.put("commits", countCommits(api, project, branches, currentIssue));
            contextMap.put("mergeRequests", countMergeRequests(api, project, issuekey));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return contextMap;
    }

    private GitlabProject getProject(GitlabAPI api, Issue currentIssue) throws IOException {
        return api.getProjects().stream().filter(p -> p.getName().equals(currentIssue.getProjectObject().getKey())).findFirst().orElse(new GitlabProject());
    }

    private List<GitlabBranch> getListBranches(GitlabAPI api, GitlabProject project, Issue currentIssue) throws IOException {
        return api.getBranches(project).stream().filter(branch -> branch.getName().endsWith(currentIssue.getKey())).collect(Collectors.toList());
    }

    private Long countCommits(GitlabAPI api, GitlabProject project, List<GitlabBranch> branches, Issue currentIssue){
        return branches.stream().flatMap(b -> {
            try {
                return api.getAllCommits(project.getId(), b.getName())
                        .stream()
                        .filter(a -> a.getMessage().trim().equals(currentIssue.getKey())
                                || a.getMessage().trim().endsWith(currentIssue.getKey())
                                || (a.getMessage().trim().length() > currentIssue.getKey().length()
                                && a.getMessage().trim().startsWith(currentIssue.getKey())
                                && a.getMessage().trim().charAt(currentIssue.getKey().length()) == ' ')
                        );
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        }).count();
    }

    private Long countMergeRequests(GitlabAPI api, GitlabProject project, String issuekey) throws IOException {
        return api.getAllMergeRequests(project).stream().filter(m -> m.getSourceBranch().endsWith(issuekey)).count();
    }
}