package ch.insign.gitlabplugin.models;

public class BranchModel {

    private String projectName;
    private String branchName;
    private String mergeStatus;

    public BranchModel() {
    }

    public BranchModel(String projectName, String branchName, String mergeStatus) {
        this.projectName = projectName;
        this.branchName = branchName;
        this.mergeStatus = mergeStatus;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getMergeStatus() {
        return mergeStatus;
    }

    public void setMergeStatus(String mergeStatus) {
        this.mergeStatus = mergeStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BranchModel that = (BranchModel) o;

        return branchName.equals(that.branchName);
    }

    @Override
    public int hashCode() {
        return branchName.hashCode();
    }
}

