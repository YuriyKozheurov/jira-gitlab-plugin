package ch.insign.gitlabplugin.models;

import net.java.ao.Entity;

public interface GitlabSettings extends Entity
{
    String getUsername();

    void setUsername(String description);

    String getPassword();

    void setPassword(String description);

    String getUrl();

    void setUrl(String description);
}
