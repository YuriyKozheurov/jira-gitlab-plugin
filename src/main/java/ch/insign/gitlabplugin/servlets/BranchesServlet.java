package ch.insign.gitlabplugin.servlets;

import java.util.*;
import java.io.IOException;
import java.net.URI;
import java.util.stream.Collectors;
import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ch.insign.gitlabplugin.models.BranchModel;
import ch.insign.gitlabplugin.models.GitlabSettings;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import javax.inject.Inject;

import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.gitlab.api.GitlabAPI;
import org.gitlab.api.models.*;

import static com.google.common.base.Preconditions.checkNotNull;


@Scanned
public class BranchesServlet extends HttpServlet
{
    private String url = "";
    private String login = "";
    private String password = "";

    @ComponentImport
    private final ActiveObjects ao;
    @ComponentImport
    private final UserManager userManager;
    @ComponentImport
    private final LoginUriProvider loginUriProvider;
    @ComponentImport
    private final TemplateRenderer templateRenderer;
    @ComponentImport
    private final PluginSettingsFactory pluginSettingsFactory;

    @Inject
    public BranchesServlet(UserManager userManager, LoginUriProvider loginUriProvider, TemplateRenderer templateRenderer, PluginSettingsFactory pluginSettingsFactory, ActiveObjects ao) {
        this.userManager = userManager;
        this.loginUriProvider = loginUriProvider;
        this.templateRenderer = templateRenderer;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.ao = checkNotNull(ao);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {

        String username = userManager.getRemoteUsername(request);
        if (username == null || !userManager.isSystemAdmin(username))
        {
            redirectToLogin(request, response);
            return;
        }

        Map<String, Object> context = new HashMap<String, Object>();

        GitlabSettings[] gitlabSettings = ao.find(GitlabSettings.class);

        if(gitlabSettings.length > 0){
            url = gitlabSettings[0].getUrl();
            login = gitlabSettings[0].getUsername();
            password = gitlabSettings[0].getPassword();
        }

        GitlabSession session = GitlabAPI.connect(url, login, password);
        GitlabAPI api = GitlabAPI.connect(url, session.getPrivateToken());

        String issuekey = request.getParameter("issuekey");
        String projectkey = request.getParameter("projectkey");

        GitlabProject project = getProject(api, projectkey);

        List<BranchModel> branchModelList = getBranchModelList(api, project, issuekey);

        context.put("api", api);
        context.put("project", project);
        context.put("branches", branchModelList);

        response.setContentType("text/html;charset=utf-8");

        templateRenderer.render("templates/branches.vm", context, response.getWriter());

    }

    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }

    private URI getUri(HttpServletRequest request)
    {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null)
        {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }

    private List<GitlabBranch> getListBranches(GitlabAPI api, GitlabProject project, String projectkey) throws IOException {
        return api.getBranches(project).stream().filter(branch -> branch.getName().endsWith(projectkey)).collect(Collectors.toList());
    }

    private List<BranchModel> getBranchModelList(GitlabAPI api, GitlabProject project, String issuekey) throws IOException {

        List<GitlabMergeRequest> mergeRequests = api.getMergeRequests(project);
        List<GitlabBranch> branches = getListBranches(api, project, issuekey);

        Set<BranchModel> branchModels = new HashSet<>();


        for (GitlabBranch branch: branches){
            for (GitlabMergeRequest merge: mergeRequests){
                if (branch.getName().equals(merge.getSourceBranch()) && merge.getState().equals("merged"))
                    branchModels.add(new BranchModel(project.getName(), branch.getName(), merge.getState()));
                else if (branch.getName().equals(merge.getSourceBranch()) && merge.getState().equals("closed"))
                    branchModels.add(new BranchModel(project.getName(), branch.getName(), merge.getState()));
                else if (branch.getName().equals(merge.getSourceBranch()) && !merge.getState().equals("merged") && !merge.getState().equals("closed"))
                    branchModels.add(new BranchModel(project.getName(), branch.getName(), "opened"));
            }
        }

        branches.forEach(b -> branchModels.add(new BranchModel(project.getName(), b.getName(), "")));

        List<BranchModel> listBranchModels = new ArrayList<>(branchModels);
        listBranchModels.sort(Comparator.comparing(BranchModel::getMergeStatus).reversed());

        return listBranchModels;
    }

    private GitlabProject getProject(GitlabAPI api, String issuekey) throws IOException {
        return api.getProjects().stream().filter(p -> p.getName().equals(issuekey)).findFirst().orElse(new GitlabProject());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(req, response);
    }
}