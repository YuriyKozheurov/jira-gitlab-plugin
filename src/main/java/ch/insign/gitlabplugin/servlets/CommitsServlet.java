package ch.insign.gitlabplugin.servlets;

import ch.insign.gitlabplugin.models.GitlabSettings;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.gitlab.api.GitlabAPI;
import org.gitlab.api.models.GitlabBranch;
import org.gitlab.api.models.GitlabCommit;
import org.gitlab.api.models.GitlabProject;
import org.gitlab.api.models.GitlabSession;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;


@Scanned
public class CommitsServlet extends HttpServlet
{

    private String url = "";
    private String login = "";
    private String password = "";

    @ComponentImport
    private final ActiveObjects ao;
    @ComponentImport
    private final UserManager userManager;
    @ComponentImport
    private final LoginUriProvider loginUriProvider;
    @ComponentImport
    private final TemplateRenderer templateRenderer;
    @ComponentImport
    private final PluginSettingsFactory pluginSettingsFactory;

    @Inject
    public CommitsServlet(UserManager userManager, LoginUriProvider loginUriProvider, TemplateRenderer templateRenderer, PluginSettingsFactory pluginSettingsFactory, ActiveObjects ao) {
        this.userManager = userManager;
        this.loginUriProvider = loginUriProvider;
        this.templateRenderer = templateRenderer;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.ao = checkNotNull(ao);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        String username = userManager.getRemoteUsername(request);
        if (username == null || !userManager.isSystemAdmin(username))
        {
            redirectToLogin(request, response);
            return;
        }

        String issuekey = request.getParameter("issuekey");
        String projectkey = request.getParameter("projectkey");

        Map<String, Object> context = new HashMap<String, Object>();

        GitlabSettings[] gitlabSettings = ao.find(GitlabSettings.class);

        if(gitlabSettings.length > 0){
            url = gitlabSettings[0].getUrl();
            login = gitlabSettings[0].getUsername();
            password = gitlabSettings[0].getPassword();
        }

        GitlabSession session = GitlabAPI.connect(url, login, password);
        GitlabAPI api = GitlabAPI.connect(url, session.getPrivateToken());

        GitlabProject project = getProject(api, projectkey);

        List<GitlabBranch> branchModelList = getListBranches(api, project, issuekey);
        List<GitlabCommit> commits = getListCommits(api, project, branchModelList, issuekey);

        context.put("api", api);
        context.put("project", project);
        context.put("commits", commits);

        response.setContentType("text/html;charset=utf-8");

        templateRenderer.render("templates/commits.vm", context, response.getWriter());

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(req, response);
    }

    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }

    private URI getUri(HttpServletRequest request)
    {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null)
        {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }

    private List<GitlabCommit> getListCommits(GitlabAPI api, GitlabProject project, List<GitlabBranch> branches, String issuekey){
        return branches.stream().flatMap(b -> {
            try {
                return api.getAllCommits(project.getId(), b.getName())
                        .stream()
                        .filter(a -> a.getMessage().trim().equals(issuekey)
                                || (a.getMessage().trim().length() > issuekey.length()
                                && a.getMessage().trim().startsWith(issuekey)
                                && a.getMessage().trim().charAt(issuekey.length()) == ' '));
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        })
                .sorted(Comparator.comparing(GitlabCommit::getCreatedAt))
                .collect(Collectors.toList());
    }

    private List<GitlabBranch> getListBranches(GitlabAPI api, GitlabProject project, String issuekey) throws IOException {
        return api.getBranches(project).stream().filter(branch -> branch.getName().endsWith(issuekey)).collect(Collectors.toList());
    }

    private GitlabProject getProject(GitlabAPI api, String issuekey) throws IOException {
        return api.getProjects().stream().filter(p -> p.getName().equals(issuekey)).findFirst().orElse(new GitlabProject());
    }
}