package ch.insign.gitlabplugin.servlets;

import ch.insign.gitlabplugin.models.GitlabSettings;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.sql.SQLException;
import java.util.*;

import static com.google.common.base.Preconditions.checkNotNull;


@Scanned
public class GitlabSettingsServlet extends HttpServlet
{
    @ComponentImport
    private final ActiveObjects ao;
    @ComponentImport
    private final UserManager userManager;
    @ComponentImport
    private final LoginUriProvider loginUriProvider;
    @ComponentImport
    private final TemplateRenderer templateRenderer;
    @ComponentImport
    private final PluginSettingsFactory pluginSettingsFactory;

    @Inject
    public GitlabSettingsServlet(UserManager userManager, LoginUriProvider loginUriProvider, TemplateRenderer templateRenderer, PluginSettingsFactory pluginSettingsFactory, ActiveObjects ao) {
        this.userManager = userManager;
        this.loginUriProvider = loginUriProvider;
        this.templateRenderer = templateRenderer;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.ao = checkNotNull(ao);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        String username = userManager.getRemoteUsername(request);
        if (username == null || !userManager.isSystemAdmin(username))
        {
            redirectToLogin(request, response);
            return;
        }

        Map<String, Object> context = new HashMap<>();

        ao.executeInTransaction((TransactionCallback<Void>) () -> {
            GitlabSettings[] gitlabSettings = ao.find(GitlabSettings.class);
            if(gitlabSettings.length == 0){
                context.put("username", "username");
                context.put("url", "url");
            } else {
                context.put("username", gitlabSettings[0].getUsername());
                context.put("url", gitlabSettings[0].getUrl());
            }
            return null;
        });

        response.setContentType("text/html;charset=utf-8");

        templateRenderer.render("templates/gitlabsettings.vm", context, response.getWriter());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        final String username = req.getParameter("username");
        final String password = req.getParameter("password");
        final String url = req.getParameter("url");

        ao.delete();

        // (1)
        ao.executeInTransaction(() -> {
            for (GitlabSettings gitlabSettings : ao.find(GitlabSettings.class))
            {
                try {
                    gitlabSettings.getEntityManager().delete(gitlabSettings);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            final GitlabSettings gitlabSettings = ao.create(GitlabSettings.class);

            gitlabSettings.setUsername(username);
            gitlabSettings.setPassword(password);
            gitlabSettings.setUrl(url);
            gitlabSettings.save();
            return gitlabSettings;
        });

        res.sendRedirect(req.getContextPath() + "/plugins/servlet/gitlabsettings");
    }

    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }

    private URI getUri(HttpServletRequest request)
    {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null)
        {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }
}